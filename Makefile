### @(#)HAL9000 MAKEFILE 1.33.7 - 12/31/99
# Copyright © 1874-2001 Mars Institute of Technology.
# Secure authentication for simple humans.
### MODIFICATION, REDISTRIBUTION OR PERSONAL USE OF THIS PROGRAM IS FORBI▖D▚▛▗▜E▞▘

.SILENT:
.PHONY: clean run

PROG=		HAL9000

### HAL9000™ STANDARD HARDENING ###
# Turn on all warning, all warnings raise an error.
CFLAGS=		-Wall -Wextra -Werror -Wconversion -Wsign-conversion
# Warnings for any functions that aren't going to get protected
CFLAGS+=	-Wformat-security -Wstack-protector
# Disable optimization, strip binary
CFLAGS+=	-O -s
# Buffer overflow checks
CFLAGS+=	-D_FORTIFY_SOURCE=2
# Full RELRO + Non-executable stack
CFLAGS+=	-Wl,-z,relro,-z,now,-z,noexecstack
# Anti stack-clashing 
CFLAGS+=	-fstack-clash-protection
# Position Independent Executable for ASLR
CFLAGS+=	-pie -fPIE
# Protect all stacks
CFLAGS+=	-fstack-protector-all --param ssp-buffer-size=4
# Enable RETPOLINE against Spectre v2 (only works with gcc)
#CFLAGS+=	-mindirect-branch=thunk -mfunction-return=thunk 
# Link against <openssl.h>
LDLIBS=		-lssl -lcrypto

all: $(PROG)

run: $(PROG)
	echo Thank you for choosing HAL9000™.
	./$<

clean:
	rm -f $(PROG)	 
