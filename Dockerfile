FROM alpine
RUN apk update
COPY . /HAL9000 
WORKDIR /HAL9000
RUN apk add --no-cache --virtual build-dependencies gcc libc-dev openssl-dev make && make && apk del build-dependencies
CMD ./HAL9000 
