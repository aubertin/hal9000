# HAL9000
Copyright © 1874-2001 Mars Institute of Technology.

__Secure authentication for simple humans.__

**HAL9000** is _the_ next generation Human-Machine Interface.

Using **AI**, the Interface is able to deter all kinds of misconduct from _any_ human user.

## Source Code

[HAL9000.c](HAL9000.c)

_MODIFICATION, REDISTRIBUTION OR PERSONAL USE OF THIS PROGRAM IS FORBIDDEN BY THE GALACTIC LAW_

## Building

Our superior mecha-engineers _only_ rely on Docker to build **HAL9000**:

```
docker build -t hal9000/TAG .
```

Execute **HAL9000** ANYWHERE in the cloud:

```
docker run -it hal9000/TAG 
```

## Security hardening 
- No optimizations
- Stripped binary
- FORTIFY\_SOURCE=2
- Full RELRO
- Non-executable stack
- Anti stack clashing
- Position Independent Executable
- RETPOLINE

See the [Makefile](Makefile) for more compilation options, available only to non-humans.
